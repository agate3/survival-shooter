﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
 
public class PowerUpHeal : MonoBehaviour
{
    public int healValue = 50;
 
    AudioSource powerUpAudio;
    ParticleSystem hitParticles;
    GameObject player;
    PlayerHealth playerHealth;
    bool isUsed;
 
 
    void Awake ()
    {
        player = GameObject.FindGameObjectWithTag ("Player");
        playerHealth = player.GetComponent <PlayerHealth> ();
        powerUpAudio = GetComponent <AudioSource> ();
        hitParticles = GetComponentInChildren <ParticleSystem> ();
    }

    void OnTriggerEnter (Collider other)
    {
        if(other.gameObject == player && other.isTrigger == false)
        {
            GetUsed();
        }
    }
 
    public void GetUsed ()
    {
        if(isUsed)
            return;
 
        powerUpAudio.Play();
        hitParticles.Play();

        isUsed = true;
        ApplyEffect();
    }

    public void ApplyEffect ()
    {
        GetComponent <Rigidbody> ().isKinematic = true;
        playerHealth.SetHealth(Mathf.Clamp(playerHealth.currentHealth + healValue, 0, 100));
        Destroy (gameObject, 0.2f);
    }
}