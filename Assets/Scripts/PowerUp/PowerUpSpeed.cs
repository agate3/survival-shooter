using UnityEngine;
using System.Collections;
using System.Collections.Generic;
 
public class PowerUpSpeed : MonoBehaviour
{
    public int speedScale = 2;
 
    AudioSource powerUpAudio;
    ParticleSystem hitParticles;
    GameObject player;
    PlayerMovement playerMovement;
    bool isUsed;


    void Awake ()
    {
        player = GameObject.FindGameObjectWithTag ("Player");
        playerMovement = player.GetComponent <PlayerMovement> ();
        powerUpAudio = GetComponent <AudioSource> ();
        hitParticles = GetComponentInChildren <ParticleSystem> ();
    }

    void OnTriggerEnter (Collider other)
    {
        if(other.gameObject == player && other.isTrigger == false)
        {
            GetUsed();
        }
    }
 
    public void GetUsed ()
    {
        if(isUsed)
            return;
 
        powerUpAudio.Play();
        hitParticles.Play();

        isUsed = true;
        ApplyEffect();
    }

    IEnumerator ApplyEffectEnd(){
        yield return new WaitForSeconds(3);
        playerMovement.speed /= speedScale;
        Destroy (gameObject, 0.2f);
    }
 
    public void ApplyEffect ()
    {
        GetComponent <Rigidbody> ().isKinematic = true;
        playerMovement.speed *= speedScale;
        transform.position -= new Vector3(0, -100, 0);
        StartCoroutine(ApplyEffectEnd());
    }
}