using UnityEngine;

public class PowerUpFactory : MonoBehaviour, IFactory{

    GameObject player;

    [SerializeField]
    public GameObject[] PowerUps;

    public GameObject FactoryMethod(int tag)
    {
        player = GameObject.FindGameObjectWithTag ("Player");
        
        //Mendapatkan nilai random
       int spawnPointX = Random.Range(-10, 10);
       int spawnPointZ = Random.Range(-10, 10);

        GameObject powerUp = Instantiate(PowerUps[tag], player.transform.position + new Vector3(spawnPointX, 1f, spawnPointZ), Quaternion.Euler(new Vector3(-90, 0, 0)));
        return powerUp;
    }
}