using UnityEngine;

public class PowerUpManager : MonoBehaviour{
    
    public PlayerHealth playerHealth;
    public float spawnTime = 3f;

    Transform player;

    [SerializeField]
    MonoBehaviour factory;
    IFactory Factory { get { return factory as IFactory; } }

    void Start(){
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }

    void Spawn(){
        if (playerHealth.currentHealth <= 0f){
           return;
        }

       int spawnPowerUp = Random.Range(0, 2);

        Factory.FactoryMethod(spawnPowerUp);
    }
}